package indra.keren.nadyne.logger.scheduler;

import indra.keren.nadyne.logger.component.MessageSender;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;

import static indra.keren.nadyne.logger.config.ActiveMQConfig.QUEUE_LOG_LOADER;

@Component
public class LogLoader implements ApplicationRunner {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    MessageSender messageSender;

    @Override
    public void run(ApplicationArguments args) throws Exception {

    }

    @Scheduled(cron = "0 0 11 * * *")
    public void runSchedule() {
        messageSender.send(QUEUE_LOG_LOADER, new JSONObject());
    }
}
