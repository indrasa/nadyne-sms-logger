package indra.keren.nadyne.logger.queue;

import indra.keren.nadyne.logger.component.MessageSender;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.jms.Session;
import java.io.File;
import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;

import static indra.keren.nadyne.logger.config.ActiveMQConfig.QUEUE_LOG_STORE;
import static indra.keren.nadyne.logger.config.ActiveMQConfig.QUEUE_LOG_STORE_SG;


public class QueueLogStoreSG {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    NamedParameterJdbcTemplate jdbc2;
    @Autowired
    NamedParameterJdbcTemplate jdbc1;
    @Autowired
    MessageSender messageSender;
    @Autowired
    ThreadPoolTaskExecutor asyncExecutor;


    public void storeLog(@Payload String jsonObjectString, @Headers MessageHeaders headers, Message message, Session session) {
        log.info("Receive log store request to SG!");
        final JSONObject jsonReq = new JSONObject(jsonObjectString);

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("reportDate", jsonReq.get("reportDate"));
        String sql = " SELECT * FROM sms_log WHERE DATE(submit_date)=:reportDate ORDER BY submit_date ";
        List<Map<String,Object>> rows = jdbc1.queryForList(sql, params);

        int i = 0;
        int size = rows.size();
        for (Map<String,Object> row : rows) {
            i++;
            boolean retry = false;

            do {
                retry = false;
                try {
                    asyncExecutor.execute(new QueueLogStoreSGWorker(row,i, size));
                    retry = false;
                } catch (Exception e) {
                    if (e instanceof org.springframework.core.task.TaskRejectedException) {
                        try {
                            //log.info("Maximum process limit ("+i+"/"+size+") (Queue: "+asyncExecutor.getThreadPoolExecutor().getQueue().size()+", active: "+asyncExecutor.getThreadPoolExecutor().getActiveCount()+")! retrying ...!");
                            Thread.sleep(100);
                        } catch (Exception e1) {};
                        retry = true;
                    }
                }
            } while (retry);
        }
    }

    private class QueueLogStoreSGWorker implements Runnable {
        private final Map<String,Object> row;
        private final int i;
        private final int size;

        public QueueLogStoreSGWorker(Map<String,Object> row, int i, int size) {
            this.i = i;
            this.size = size;
            this.row = row;
        }

        @Override
        public void run() {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("id", row.get("id"));
            params.addValue("mobile", row.get("msisdn"));
            params.addValue("message", row.get("message"));
            params.addValue("senderId", row.get("sender_id"));
            params.addValue("submitDate", row.get("submit_date"));
            params.addValue("trxId", row.get("trx_id"));
            params.addValue("smsCount", row.get("sms_count"));
            params.addValue("carrier", row.get("carrier"));
            params.addValue("type", row.get("type"));
            params.addValue("status", row.get("status"));

            /**
            String sql = " INSERT INTO ID_SMS_Log (ID,ins_time,msisdn,message,sender_id,submit_date,status,trx_id,sms_count,carrier,type) " +
                    " SELECT :id,GETDATE(),:mobile,:message,:senderId,:submitDate,:status,:trxId,:smsCount,:carrier,:type   "+
                    //" VALUES ((SELECT MAX(ID)+1 FROM dbo.ID_SMS_Log), GETDATE(),:mobile,:message,:senderId,:submitDate,:status,:trxId,:smsCount,:carrier,:type) " +
                    " WHERE NOT EXISTS (SELECT 1 FROM ID_SMS_Log AS d WHERE d.trx_id=:trxId) ";

            **/
            String sql = " INSERT INTO ID_SMS_Log (ID,ins_time,msisdn,message,sender_id,submit_date,status,trx_id,sms_count,carrier,type) VALUES (:id,GETDATE(),:mobile,:message,:senderId,:submitDate,:status,:trxId,:smsCount,:carrier,:type) ";

            try {
                jdbc2.update(sql, params);
                log.info("Updated to SG: ("+i+"/"+size+") "+row.get("submit_date")+", "+row.get("msisdn"));
            } catch (Exception e) {
                log.error("Error while submitting data to SG: "+e,e);
            }
        }
    }
}
