package indra.keren.nadyne.logger.queue;

import indra.keren.nadyne.logger.component.Mailer;
import indra.keren.nadyne.logger.component.MessageSender;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import javax.jms.Session;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.invoke.MethodHandles;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static indra.keren.nadyne.logger.config.ActiveMQConfig.QUEUE_LOG_SEND;
import static indra.keren.nadyne.logger.config.ActiveMQConfig.QUEUE_LOG_STORE;

@Component
public class QueueSendReport {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    NamedParameterJdbcTemplate jdbc1;
    @Autowired
    Mailer mailer;
    @Autowired
    MessageSender messageSender;

    @JmsListener(destination = QUEUE_LOG_SEND)
    public void sendLog(@Payload String jsonObjectString, @Headers MessageHeaders headers, Message message, Session session) {
        JSONObject jsonReq = new JSONObject(jsonObjectString);
        log.info("Start sending report date: "+jsonReq.getString("reportDate"));

        try {
            QueueSendReportWorker worker = new QueueSendReportWorker(jsonReq.getString("reportDate"));
            worker.run();
        } catch (Exception e) {
            log.error("Error sending report: "+e,e);
        }

    }

    private class QueueSendReportWorker {
        private final String reportDate;
        final Workbook wbReport;
        final Sheet sheet;
        private CellStyle defaultStyle;
        int rowNum = 0;

        private QueueSendReportWorker(String reportDate) {
            this.reportDate = reportDate;
            wbReport = new XSSFWorkbook();
            sheet = wbReport.createSheet();
        }

        private void initWorkbook() throws Exception {
            defaultStyle = wbReport.createCellStyle();
            defaultStyle.setWrapText(true);
            defaultStyle.setVerticalAlignment(VerticalAlignment.TOP);

            Row headerRow = sheet.createRow(rowNum);
            headerRow.createCell(0).setCellValue("ID#");
            headerRow.createCell(1).setCellValue("SMS Type");
            headerRow.createCell(2).setCellValue("Date/Time");
            headerRow.createCell(3).setCellValue("Mobile");
            headerRow.createCell(4).setCellValue("Message");
            headerRow.createCell(5).setCellValue("Status");
            headerRow.createCell(6).setCellValue("Sender ID");
            headerRow.createCell(7).setCellValue("Carrier");
            headerRow.createCell(8).setCellValue("SMS Count");
            headerRow.createCell(9).setCellValue("Trx ID");
            headerRow.createCell(10).setCellValue("UserName");

            sheet.setColumnWidth(0, 2000);
            sheet.setColumnWidth(1, 5000);
            sheet.setColumnWidth(2, 6000);
            sheet.setColumnWidth(3, 4000);
            sheet.setColumnWidth(4, 14000);
            sheet.setColumnWidth(5, 4000);
            sheet.setColumnWidth(6, 4000);
            sheet.setColumnWidth(7, 4000);
            sheet.setColumnWidth(8, 3000);
            sheet.setColumnWidth(9, 7000);

        }

        private void run() throws Exception {
            initWorkbook();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date rawDate = df.parse(reportDate);

            DateTime dt1 = new DateTime(rawDate);
            DateTime dt2 = dt1.plusDays(1);

            log.info("Date #1: "+dt1.toString()+", Date #2: "+dt2);

            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("dt1", dt1.toString("yyyy-MM-dd HH:mm:ss"));
            params.addValue("dt2", dt2.toString("yyyy-MM-dd HH:mm:ss"));

            String sql = " SELECT * FROM sms_log WHERE submit_date>=:dt1 AND submit_date<:dt2 ORDER BY submit_date,id ";
            List<Map<String,Object>> rows = jdbc1.queryForList(sql, params);
            for (Map<String,Object> row: rows) {
                JSONObject jsonRow = new JSONObject(row);
                rowNum++;
                Row cellRow = sheet.createRow(rowNum);
                Cell cell0 = cellRow.createCell(0); cell0.setCellStyle(defaultStyle); cell0.setCellValue(jsonRow.getInt("id"));
                Cell cell1 = cellRow.createCell(1); cell1.setCellStyle(defaultStyle); cell1.setCellValue(jsonRow.getString("type"));
                Cell cell2 = cellRow.createCell(2); cell2.setCellStyle(defaultStyle); cell2.setCellValue(jsonRow.getString("submit_date"));
                Cell cell3 = cellRow.createCell(3); cell3.setCellStyle(defaultStyle); cell3.setCellValue(jsonRow.getString("msisdn"));
                Cell cell4 = cellRow.createCell(4); cell4.setCellStyle(defaultStyle); cell4.setCellValue(jsonRow.getString("message"));
                Cell cell5 = cellRow.createCell(5); cell5.setCellStyle(defaultStyle); cell5.setCellValue(jsonRow.getString("status"));
                Cell cell6 = cellRow.createCell(6); cell6.setCellStyle(defaultStyle); cell6.setCellValue(jsonRow.getString("sender_id"));
                Cell cell7 = cellRow.createCell(7); cell7.setCellStyle(defaultStyle); cell7.setCellValue(jsonRow.getString("carrier"));
                Cell cell8 = cellRow.createCell(8); cell8.setCellStyle(defaultStyle); cell8.setCellValue(jsonRow.getInt("sms_count"));
                Cell cell9 = cellRow.createCell(9); cell9.setCellStyle(defaultStyle); cell9.setCellValue(jsonRow.getString("trx_id"));
                Cell cell10 = cellRow.createCell(10); cell10.setCellStyle(defaultStyle); cell10.setCellValue(jsonRow.getString("username"));
            }

            sheet.setZoom(80);

            String reportFileName = "SMS-REPORT (NADYNE) - "+reportDate+".xlsx";
            String reportFolder = "/home/danafix/sms/report/"+dt1.getYear()+"/"+String.format("%02d", dt1.getMonthOfYear())+"/";
            try {
                File folder = new File(reportFolder);
                folder.mkdirs();
            } catch (Exception e) {}

            File reportFile = new File(reportFolder+reportFileName);
            FileOutputStream outputStream = new FileOutputStream(reportFile);
            wbReport.write(outputStream);
            wbReport.close();

            log.info("Workbook created! "+reportFile.getAbsolutePath());

            List<String> tos = new ArrayList<>();
            tos.add("sms.report@danafix.id");
            List<File> attachments = new ArrayList<>();
            attachments.add(reportFile);
            String body = "<p>Generated at: "+DateTime.now().toString("yyyy-MM-dd HH:mm:ss")+"</p>";
            mailer.send(tos,"SMS Report - "+dt1.toString("yyyy-MM-dd"), body, attachments);
        }

        private String generateEmailBody() {
            return "";
        }
    }
}
