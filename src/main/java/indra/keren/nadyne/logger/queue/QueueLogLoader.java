package indra.keren.nadyne.logger.queue;

import indra.keren.nadyne.logger.component.HttpUtils;
import indra.keren.nadyne.logger.component.MessageSender;
import indra.keren.nadyne.logger.component.NadyneLogRetriever;
import indra.keren.nadyne.logger.component.ZipUtils;
import org.apache.commons.text.StringSubstitutor;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import javax.jms.Session;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.invoke.MethodHandles;
import java.util.*;

import static indra.keren.nadyne.logger.config.ActiveMQConfig.QUEUE_LOG_LOADER;
import static indra.keren.nadyne.logger.config.ActiveMQConfig.QUEUE_LOG_STORE;

@Component
public class QueueLogLoader {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Value("${nadyne.url.login}") private String nadyneUrlLogin;
    @Value("${nadyne.url.success}") private String nadyneUrlSuccess;
    @Value("${nadyne.url.download}") private String nadyneUrlDownload;

    @Value("${nadyne.login.username}") private String username;
    @Value("${nadyne.login.password}") private String password;
    @Value("${nadyne.login.field.username}") private String usernameField;
    @Value("${nadyne.login.field.password}") private String passwordField;

    @Autowired
    HttpUtils httpUtils;
    @Autowired
    ZipUtils zipUtils;
    @Autowired
    MessageSender messageSender;


    @JmsListener(destination = QUEUE_LOG_LOADER)
    public void loadLog(@Payload String jsonObjectString, @Headers MessageHeaders headers, Message message, Session session) {
        log.info("Receive log loader request!");

        JSONObject jsonReq = new JSONObject(jsonObjectString);
        Calendar cal1 = Calendar.getInstance();

        int pastDays = 1;

        if (jsonReq.has("pastDays")) pastDays = jsonReq.getInt("pastDays");
        cal1.add(Calendar.HOUR, (0-pastDays)*24);
        QueueLogLoaderWorker worker = new QueueLogLoaderWorker(pastDays);
        worker.start();
    }

    private class QueueLogLoaderWorker {
        private String cookie;
        private File downloadedFile;
        private String dt1;
        private String dt2;
        private final Calendar cal1;

        private QueueLogLoaderWorker (int pastDays) {
            cal1 = Calendar.getInstance();
            cal1.add(Calendar.HOUR, (0-pastDays)*24);
            dt1 = (new DateTime(cal1.getTime())).toString("yyyy-MM-dd");
            cal1.add(Calendar.HOUR, 24);
            dt2 = (new DateTime(cal1.getTime())).toString("yyyy-MM-dd");
            log.info("Period: "+dt1+" - "+dt2);
        }

        private void start() {
            try {
                login();
                download();
            } catch (Exception e) {
                log.error("Error while retrieving log from Nadyne: "+e,e);
            }
        }

        private void login() throws Exception {
            log.info("Login URL: "+nadyneUrlLogin);
            HttpPost httpPost = new HttpPost(nadyneUrlLogin);
            List<NameValuePair> params = new ArrayList<>();

            BasicNameValuePair fieldUsername = new BasicNameValuePair(usernameField, username);
            BasicNameValuePair fieldPassword = new BasicNameValuePair(passwordField, password);

            log.info(fieldUsername.getName()+": "+fieldUsername.getValue());
            log.info(fieldUsername.getName()+": "+fieldUsername.getValue());

            params.add(fieldUsername);
            params.add(fieldPassword);
            params.add(new BasicNameValuePair("yt0", "Submit"));

            UrlEncodedFormEntity encodedFormData = new UrlEncodedFormEntity(params, "UTF-8");

            httpPost.setEntity(encodedFormData);
            CloseableHttpResponse response = httpUtils.getHttpClient().execute(httpPost);

            Header[] headers = response.getAllHeaders();
            for (int i=0;i<headers.length;i++) {
                log.info("Receive response header #"+i+" \""+headers[i].getName()+"\":\""+headers[i].getValue()+"\"");
            }

            cookie = response.getLastHeader("Set-Cookie").getValue();
            log.info("Cookie retrived: "+cookie);

            String successLocation = (response.getLastHeader("Location")!=null)?response.getLastHeader("Location").getValue():null;
            if (successLocation == null || !successLocation.equals(nadyneUrlSuccess)) {
                throw new Exception("Location is null or not matched with success login parameter! Location: "+successLocation);
            }
            log.info("Redirect to: "+successLocation);

            httpUtils.close(response);
        }

        private void download() throws Exception {
            log.info("Download report URL template: "+nadyneUrlDownload);
            Map<String,String> replacement = new HashMap<>();
            replacement.put("dt1", dt1);
            replacement.put("dt2", dt1);
            StringSubstitutor substitutor = new StringSubstitutor(replacement);
            String url = substitutor.replace(nadyneUrlDownload);
            log.info("Download report URL (actual): "+url);

            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Cookie", cookie);

            CloseableHttpResponse response = httpUtils.getHttpClient().execute(httpGet);

            Header[] headers = response.getAllHeaders();
            for (int i=0;i<headers.length;i++) {
                log.debug("Receive response header #"+i+" \""+headers[i].getName()+"\":\""+headers[i].getValue()+"\"");
            }

            if (!response.getLastHeader("Content-type").getValue().equals("application/zip")) {
                log.warn("Download failed, content is not zip file!");
                throw new Exception("Downloaded file is not zip file!");
            }

            BufferedInputStream bis = new BufferedInputStream(response.getEntity().getContent());
            File zipFile = new File("/tmp/sms-report.zip");
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(zipFile));
            int inByte;
            while((inByte = bis.read()) != -1) bos.write(inByte);
            bis.close();
            bos.close();

            ArrayList<File> extractedFiles = zipUtils.unZip(zipFile, System.getProperty("java.io.tmpdir"));
            if (extractedFiles.size()<=0) {
                log.warn("No files were extracted!");
                throw new Exception("No extracted files at all!");
            }

            for (File csvFile : extractedFiles) {
                JSONObject jsonReq = new JSONObject();
                jsonReq.put("csvFile", csvFile.getAbsolutePath());
                jsonReq.put("date", dt1);
                messageSender.send(QUEUE_LOG_STORE, jsonReq);
            }

            httpUtils.close(response);
        }
    }
}
