package indra.keren.nadyne.logger.queue;

import com.google.i18n.phonenumbers.PhoneNumberToCarrierMapper;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.opencsv.CSVReader;
import indra.keren.nadyne.logger.component.HttpUtils;
import indra.keren.nadyne.logger.component.MessageSender;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.jms.Session;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.invoke.MethodHandles;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.Locale;

import static indra.keren.nadyne.logger.config.ActiveMQConfig.*;

@Component
public class QueueLogStore {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    NamedParameterJdbcTemplate jdbc1;
    @Autowired
    MessageSender messageSender;
    @Autowired
    HttpUtils httpUtils;

    int i = 0;

    private static final ThreadPoolTaskExecutor taskExecutor;

    static {
        taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setMaxPoolSize(30);
        taskExecutor.setQueueCapacity(5);
        taskExecutor.setThreadNamePrefix("CONTRACT-AGENT-");
        taskExecutor.initialize();
    }

    @JmsListener(destination = QUEUE_LOG_STORE)
    public void storeLog(@Payload String jsonObjectString, @Headers MessageHeaders headers, Message message, Session session) {
        log.info("Receive log store request!");

        JSONObject jsonReq = new JSONObject(jsonObjectString);
        File csvFile = new File(jsonReq.getString("csvFile"));
        if (!csvFile.exists()) {
            log.info("CSV File doesnt exists!!!");
            return;
        }
        log.info("Processing csv file: "+csvFile.getAbsolutePath());
        QueueLogStoreWorker worker = new QueueLogStoreWorker(csvFile, jsonReq.getString("date"));
        worker.run();
    }

    private class QueueLogStoreWorker {
        private final File csvFile;
        private final String reportDate;

        private QueueLogStoreWorker(File csvFile, String reportDate) {
            this.csvFile = csvFile;
            this.reportDate = reportDate;
        }

        public void run() {
            log.info("Processing CSV file: "+csvFile.getAbsolutePath()+", report date: "+reportDate);
            try {
                parseAndStore();
            } catch (Exception e) {
                log.error("Error parse and store csv: "+e,e);
            }
        }

        private void parseAndStore() throws Exception {
            try {

                String sql = " INSERT IGNORE INTO sms_log (ins_time,msisdn,message,sender_id,submit_date,status,trx_id,sms_count,carrier,type,username) " +
                        " VALUES (NOW(),:mobile,:message,:senderId,:submitDate,:status,:trxId,:smsCount,:carrier,:type,:userName) ";
                // Create an object of filereader
                // class with CSV file as a parameter.
                FileReader filereader = new FileReader(csvFile);

                // create csvReader object passing
                // file reader as a parameter
                CSVReader csvReader = new CSVReader(filereader);
                String[] record;

                // we are going to read data line by line
                i=0;
                while ((record = csvReader.readNext()) != null) {
                    i++;
                    if (i>1) try {
                        String userName = record[0];
                        String trxId = record[1];
                        String senderId = record[2];
                        String mobile = record[3];
                        String content = record[4];
                        String submitDate = record[5];
                        String smsCount = record[6];

                        //log.info("Username: "+userName+", mobile: "+mobile);

                        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                        Phonenumber.PhoneNumber msisdn = phoneUtil.parse(mobile,"ID");
                        PhoneNumberToCarrierMapper carrierMapper = PhoneNumberToCarrierMapper.getInstance();
                        String carrier = carrierMapper.getNameForNumber(msisdn, Locale.ENGLISH);

                        if (carrier.equals("3")) carrier = "TRI (3)";

                        String type = getType(content, trxId);
                        MapSqlParameterSource params = new MapSqlParameterSource();
                        params.addValue("mobile", mobile);
                        params.addValue("message", content);
                        params.addValue("senderId", senderId);
                        params.addValue("submitDate", submitDate);
                        params.addValue("trxId", trxId);
                        params.addValue("smsCount", smsCount);
                        params.addValue("carrier", carrier);
                        params.addValue("type", type);
                        params.addValue("status", "SENT");
                        params.addValue("userName", userName);

                        boolean retry = false;
                        do {
                            retry = false;
                            try {
                                log.info("Storing #" + i + " " + mobile + ", " + submitDate);
                                taskExecutor.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        jdbc1.update(sql, params);
                                    }
                                });
                            } catch (org.springframework.core.task.TaskRejectedException e) {
                                try {
                                    Thread.sleep(1000);
                                    retry = true;
                                } catch (Exception ex) {}
                            } catch (Exception e) {
                                log.error("Error storing to database: "+e);
                            }
                        } while (retry);


                    } catch (Exception e) {
                        log.error("Process record "+e,e);
                    }
                }

                log.info("Storing to database was completed!");
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void parseAndStore2() throws Exception {



            try (BufferedReader reader = new BufferedReader(new FileReader(csvFile), 1048576 * 30)) {
                Iterable<CSVRecord> records = CSVFormat.RFC4180.parse(reader);
                i = 0;
                for (CSVRecord record : records) {
                    // Process each line here

                    try {
                        String userName = record.get(0);
                        String trxId = record.get(1);
                        String senderId = record.get(2);
                        String mobile = record.get(3);
                        String content = record.get(4);
                        String submitDate = record.get(5);
                        String smsCount = record.get(6);


                    } catch (Exception ex) {

                    }
                    i++;
                    //if (i>10) break;
                }
            }
            catch (Exception e) {
                log.error("Error : "+e,e);
            }



            /**
            CSVParser parser = CSVParser.parse(csvFile, Charset.forName("UTF-8") , CSVFormat.RFC4180.withHeader());
            log.info("Storing data to database ....");

            int i = 0;
            for (CSVRecord record : parser) {
                String userName = record.get("Username");
                String mobile = record.get("MSISDN");
                String content = record.get("content SMS");

                PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                Phonenumber.PhoneNumber msisdn = phoneUtil.parse(mobile,"ID");
                PhoneNumberToCarrierMapper carrierMapper = PhoneNumberToCarrierMapper.getInstance();
                String carrier = carrierMapper.getNameForNumber(msisdn, Locale.ENGLISH);

                if (carrier.equals("3")) carrier = "TRI (3)";

                String trxId = record.get("TRX ID");
                String type = getType(content, trxId);
                MapSqlParameterSource params = new MapSqlParameterSource();
                params.addValue("mobile", mobile);
                params.addValue("message", content);
                params.addValue("senderId", record.get("Sender ID"));
                params.addValue("submitDate", record.get("TRX Date"));
                params.addValue("trxId", trxId);
                params.addValue("smsCount", record.get("Total SMS"));
                params.addValue("carrier", carrier);
                params.addValue("type", type);
                params.addValue("status", "SENT");
                params.addValue("userName", userName);

                try {
                    jdbc1.update(sql, params);
                    log.info("Storing #"+i+" "+mobile+", "+record.get("TRX Date"));
                } catch (Exception e) {
                    log.error("Error storing to database: "+e);
                }

                i++;
                //if (i>10) break;
            }

             **/



            JSONObject jsonReq = new JSONObject();
            jsonReq.put("reportDate", reportDate);
            //messageSender.send(QUEUE_LOG_STORE_SG, jsonReq);
            messageSender.send(QUEUE_LOG_SEND, jsonReq);
        }

        private String getType(String message, String trxId) {
            // new format
            if (message.matches("^(?i:Selamat! Pinjaman Anda telah di transfer di rekening Anda)(.*)"))  return "PAYMENT";
            if (message.matches("^(?i:Selamat! Pinjaman telah di transfer di rekening Anda)(.*)"))  return "DISBURSEMENT";
            if (message.matches("^(?i:Terima kasih utk pembayaran Anda! Nikmati jumlah lebih besar)(.*)"))  return "REPAYMENT";
            if (message.matches("^(?i:Aplikasi Anda ditolak, silahkan mencoba kembali setelah satu bulan)(.*)"))  return "REJECTION";
            if (message.matches("^(?i:Dengan menyesal, Doctor Rupiah menginformasikan bahwa aplikasi pinjaman Anda tidak sesuai dengan persyaratan dari pemberi pinjaman yang ada)(.*)"))  return "DUPLICATION/BAD SCAN";
            if (message.matches("^(?i:Sekarang Anda bisa mendapatkan pinjaman Rp)([0-9]{1,8})(.*)(hanya 3 klik di)(.*)"))  return "REPEAT1";
            if (message.matches("^(?i:Khusus nasabah terpilih, kami berikan pinjaman sebesar Rp)([0-9]{1,8})(.*)(dalam 3 klik di)(.*)"))  return "REPEAT3";
            if (message.matches("^(?i:Dapatkan Rp)([0-9]{1,8})(.*)(dlm 3 klik ,nikmati akhir pekan! Kesempatan ini utk Anda! Klik)(.*)"))  return "REPEAT2";
            if (message.matches("^(?i:Spesial untuk Anda! Dapatkan pinjaman Rp)([0-9]{1,8})(.*)(dalam 3 klik di)(.*)"))  return "REPEAT4";
            if (message.matches("^(?i:Penuhi kebutuhan darurat dgn pinjaman dari DRRUPIAH! Hanya dgn 3 klik di)(.*)"))  return "DORMANT";
            if (message.matches("^(?i:Pelanggan YTH, pinjaman Anda tidak dapat dicairkan, mohon untuk melakukan pengajuan ulang, dan pastikan nama dan nomor rekening benar)(.*)"))  return "INVALID BANK ACCOUNT";
            if (message.matches("^(?i:SMS ini memastikan bahwa Anda janji bayar Rp)([0-9]{1,8})(.*)"))  return "COLLECTION PTP";
            if (message.matches("^(?i:Selamat! Pengajuan Anda telah terkirim & akan diproses dlm 1-2 hari. Ingin peluang disetujui 30% lebih besar)(.*)"))  return "SLYP CAMPAIGN";
            if (message.matches("^(?i:Hai, selangkah lagi Anda menyelesaikan proses aplikasi DRRUPIAH. Selesaikan sekarang untuk terima pinjaman segera)(.*)"))  return "UNFINISHED CAMPAIGN";
            // older format
            if (message.matches("^(?i:kode konfirmasi dari aplikasi peminjaman)(.*)")) return "WEB OTP1";
            if (message.matches("^(?i:Dengan memasukkan kode ini )([0-9]{4,6})(.*)"))  return "WEB OTP2";
            if (message.matches("^(?i:By entering this code )([0-9]{4,6})(.*)"))  return "WEB OTP4";
            if (message.matches("([0-9]{4,6})"))  return "WEB OTP3";
            if (message.matches("^(?i:Terima kasih atas aplikasi Anda. Aplikasi anda belum bisa disetujui)(.*)"))  return "REJECTION1";
            if (message.matches("^(?i:Dengan menyesal, Doctor Rupiah menginformasikan bahwa aplikasi pinjaman Anda tidak sesuai dengan persyaratan dari pemberi pinjaman yang ada)(.*)"))  return "REJECTION2";
            if (message.matches("^(?i:Kerabat Anda dpt pinjaman DRRUPIAH dan Anda jg bs! Ajukan online)(.*)"))  return "SPOUSE CAMPAIGN1";
            if (message.matches("^(?i:Terima kasih atas pembayarannya. Sekarang Anda bisa menggunakan pinjaman sampai)(.*)"))  return "PROLONGATION";
            if (message.matches("^(?i:Terima kasih utk pembayaran Anda! Nikmati jumlah lebih besar Rp )([0-9]{1,8})(.*)"))  return "PAYMENT2";
            if (message.matches("^(?i:Yth)(.*)(, terima kasih, pinjaman Anda telah berhasil dibayar. www.drrupiah.com)"))  return "PAYMENT3";
            if (message.matches("^(?i:SMS ini mengingatkan bahwa Anda janji bayar hari ini Rp)([0-9]{1,8})(.*)"))  return "COLLECTION1";
            if (message.matches("^(?i:Nasabah Yth, lunasi pinjaman Rp)([0-9]{1,8})(.*)(Setelah pelunasan dapatkan pinjaman lagi dlm 3 menit sbsr)(.*)"))  return "COLLECTION2";
            if (message.matches("^(?i:Hindari denda dgn melunasi pinjaman hr ini Rp)([0-9]{1,8})(.*)(Biaya perpanjangan 30 hari hanya Rp)([0-9]{1,8})(.*)"))  return "COLLECTION3";
            if (message.matches("^(?i:Byr lunas Rp)([0-9]{1,8})(.*)(agar tdk ditagih Kolektor Lapangan. Perpanjangan 30hr)(.*)"))  return "COLLECTION4";
            if (message.matches("^(?i:Byr lunas hr ini Rp)([0-9]{1,8})(.*)(agar denda tidak bertambah)(.*)"))  return "COLLECTION5";
            if (message.matches("^(?i:Byr lunas Rp)([0-9]{1,8})(.*)(agar tdk ditangani Agen Penagihan)(.*)"))  return "COLLECTION6";
            if (message.matches("^(?i:SMS ini memastikan bahwa Anda janji bayar Rp)([0-9]{1,8})(.*)(Transfer ke Permata)(.*)"))  return "COLLECTION7";
            if (message.matches("^(?i:Peringatan terakhir! Lunasi hari ini Rp)([0-9]{1,8})(.*)(Transfer ke Permata)(.*)"))  return "COLLECTION8";
            if (message.matches("^(?i:Selamat! Pinjaman anda sudah dicairkan ke rekening bank Anda. Anda akan menerima pada hari kerja berikutnya\\.)"))  return "APPROVAL1";
            if (message.matches("^(?i:Selamat! Pinjaman Anda telah dicairkan ke rekening bank Anda. Anda akan menerima pada hari kerja berikutnya\\. Untuk pelunasan pinjaman mohon bayar)(.*)"))  return "APPROVAL2";
            if (message.matches("^(?i:Sekarang Anda bisa mendapatkan pinjaman IDR )([0-9]{1,8})(.*)(tanpa harus mengisi form lagi\\. Klik http://drrupiah.com/)(.*)"))  return "REPEAT1";
            if (message.matches("^(?i:Khusus untuk nasabah terpilih, kami berikan Anda pinjaman sebesar IDR )([0-9]{1,8})(.*)(Klik http://drrupiah.com/)(.*)"))  return "REPEAT2";
            if (message.matches("^(?i:Spesial untuk Anda! Dapatkan pinjaman IDR )([0-9]{1,8})(.*)(Klik http://drrupiah.com/)(.*)"))  return "REPEAT3";
            if (message.matches("^(?i:Khusus untuk nasabah terpilih, kami berikan Anda pinjaman sebesar IDR )([0-9]{1,8})(.*)(Klik http://drrupiah.com/)(.*)"))  return "REPEAT4";
            if (message.matches("^(?i:Dapatkan IDR )([0-9]{1,8})(.*)(Kesempatan ini hanya untuk nasabah terpilih seperti Anda! Klik http://drrupiah.com/)(.*)"))  return "REPEAT5";
            if (message.matches("^(?i:Penuhi kebutuhan darurat Anda dengan pinjaman dari DRRUPIAH! Dapatkan bunga rendah spesial untuk Anda! Klik http://drrupiah.com/)(.*)"))  return "REPEAT6";
            if (message.matches("^(.*)(?i:\\bYes)(.*)"))  return "PRE-APPROVAL1";
            if (StringUtils.containsIgnoreCase(message,"Selamat! Pengajuan Anda telah terkirim & akan diproses dlm 1-2 hari. Pastikan HP Anda & kerabat aktif utk kami hubungi! Tingkatkan peluang disetujui hingga 30% lebih besar! Forward atau kirim kan slip gaji asli Anda ke slip@drrupiah.com, cantumkan nomor HP Anda sebagai subjectnya"))  return "SLYP CAMPAIGN";
            if (message.matches("^(Terima pinjaman dlm 3 MENIT saat LIBURAN)(.*)"))  return "AD-HOC1";
            if (message.matches("^(?i:Khusus untuk Anda nasabah setia DRRUPIAH, tanpa isi aplikasi dapatkan dana pinjaman dalam 2)(.*)"))  return "AD-HOC2";
            if (StringUtils.containsIgnoreCase(message,"Hanya dengan 2 klik saja, Anda bisa dapat dana dalam 2 menit dari DRRUPIAH"))  return "AD-HOC3";
            if (StringUtils.containsIgnoreCase(message,"Nikmati penghapusan denda dan diskon 30% dr pokok pinjaman dan bunga.Hanya sampai tahun baru imlek"))  return "AD-HOC4";
            if (StringUtils.containsIgnoreCase(message,"Persiapkan kebutuhan long weekend Febuari 2018 lebih awal! Dapatkan pinjaman dalam 2 MENIT"))  return "AD-HOC5";
            if (StringUtils.containsIgnoreCase(message,"Long weekend butuh tambahan dana? Dapatkan pinjaman & tentukan jangka waktunya sendiri, hanya dlm"))  return "AD-HOC6";
            if (StringUtils.containsIgnoreCase(message,"Kami punya informasi penting untuk anda. Segera hubungi kami di"))  return "COLLECTION DRC1";
            if (StringUtils.containsIgnoreCase(message,"Jangan abaikan tlp kami. Tagihan anda saat ini"))  return "COLLECTION DRC2";
            if (StringUtils.containsIgnoreCase(message,"Proses transfer data kepihak Agency dimulai. Properti dan pekerjaan akan di periksa utk tindakan selanjutnya"))  return "COLLECTION DRC3";
            if (StringUtils.containsIgnoreCase(message,"Anda memberikan kewenangan kpd kami meneruskan data pribadi Anda kpd pihak lain, yang akan memberikan penawaran&pinjaman berikutnya"))  return "WEB OTP REPEAT";
            if (StringUtils.containsIgnoreCase(message,"test"))  return "TEST";
            if (StringUtils.containsIgnoreCase(message,"tes"))  return "TEST";

            //log.info("UNIDENTIFIED: "+message);

            // check in manual SMS
            try {
                return getSubjectFromManualSms(trxId);
            } catch (Exception e) {
                return "UNIDENTIFIED";
            }
        }

        private String getSubjectFromManualSms(String trxId) throws Exception {
            String url = "http://localhost:20001/nadyne/detail/ref/"+ URLEncoder.encode(trxId,"UTF-8");

            HttpGet httpGet = new HttpGet(url);
            CloseableHttpResponse httpResponse = httpUtils.getHttpClient().execute(httpGet);
            JSONObject result = new JSONObject(IOUtils.toString(httpResponse.getEntity().getContent(), StandardCharsets.UTF_8));
            if (!result.getBoolean("success")) return "UNIDENTIFIED";
            else return result.getJSONObject("data").getString("subject");
        }
    }
}
