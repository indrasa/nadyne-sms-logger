package indra.keren.nadyne.logger.queue;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;

@Component
public class ServiceLogToSG implements ApplicationRunner {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    ThreadPoolTaskScheduler taskScheduler;
    @Autowired
    NamedParameterJdbcTemplate jdbc1;
    @Autowired
    NamedParameterJdbcTemplate jdbc2;

    private static final ThreadPoolTaskExecutor taskExecutor;

    static {
        taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setMaxPoolSize(30);
        taskExecutor.setQueueCapacity(5);
        taskExecutor.setThreadNamePrefix("CONTRACT-AGENT-");
        taskExecutor.initialize();
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        try {
            MapSqlParameterSource params = new MapSqlParameterSource();

            while (true) {

                int h = DateTime.now().getHourOfDay();
                if (h < 21 && h > 5) {
                    log.info("Sleeping during business hours");
                    Thread.sleep(60000);
                    continue;
                }

                long sleep = 60000;
                String sql = " SELECT * FROM sms_log WHERE exported_on IS NULL LIMIT 0,100 ";
                List<Map<String,Object>> rows = jdbc1.queryForList(sql, params);
                for (Map<String,Object> row:rows) {
                    long id = (long) row.get("id");

                    sleep = 100;

                    boolean retry = false;
                    do {
                        retry = false;
                        try {
                            taskExecutor.execute(new ServiceLogToSGSender(row));
                            retry = false;
                        } catch (Exception e) {
                            if (e instanceof org.springframework.core.task.TaskRejectedException) {
                                try {
                                    //log.info("Maximum process limit reached (Queue: "+taskExecutor.getThreadPoolExecutor().getQueue().size()+", active: "+taskExecutor.getThreadPoolExecutor().getActiveCount()+")! retrying ...!");
                                    Thread.sleep(100);
                                } catch (Exception e1) {};
                                retry = true;
                            }
                        }

                    } while (retry);
                }
                Thread.sleep(sleep);
            }
        } catch (Exception e) {
            log.error("Error runner: "+e,e);
        }

    }

    private class ServiceLogToSGSender implements Runnable {
        private final Map<String,Object> rec;
        private final long id;
        public ServiceLogToSGSender(Map<String,Object> rec) {
            this.rec = rec;
            id = (long) rec.get("id");
        }

        @Override
        public void run() {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValues(rec);

            try {
                String sql = " UPDATE sms_log set exported_on = NOW() where id=:id ";
                jdbc1.update(sql, params);
                sql = " INSERT INTO ID_SMS_Log (ID,ins_time,msisdn,message,sender_id,submit_date,status,trx_id,sms_count,carrier,type) VALUES (:id,GETDATE(),:msisdn,:message,:sender_id,:submit_date,:status,:trx_id,:sms_count,:carrier,:type) ";
                jdbc2.update(sql, params);
                log.info("#"+id+", updated "+rec.get("submit_date"));
            } catch (Exception e) {
                if (e instanceof org.springframework.dao.DuplicateKeyException) {

                } else {
                    String sql = " UPDATE sms_log set exported_on = NULL where id=:id ";
                    jdbc1.update(sql, params);
                }
                //String sql = " UPDATE sms_log set exported_on = NOW() where id=:id ";
                log.error("Error while updating to SG: "+e,e);
            }
        }
    }



}
