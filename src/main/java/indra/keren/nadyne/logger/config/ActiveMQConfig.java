package indra.keren.nadyne.logger.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;

@EnableJms
@Configuration
public class ActiveMQConfig {
    public static final String QUEUE_LOG_LOADER = "queue-reload";
    public static final String QUEUE_LOG_STORE = "queue-store";
    public static final String QUEUE_LOG_STORE_SG = "queue-store-sg";
    public static final String QUEUE_LOG_SEND = "queue-send";


}
