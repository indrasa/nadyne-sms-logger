package indra.keren.nadyne.logger.controller;

import indra.keren.nadyne.logger.component.MessageSender;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;

import static indra.keren.nadyne.logger.config.ActiveMQConfig.QUEUE_LOG_LOADER;

@RestController
public class RestApi {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    MessageSender messageSender;
    @Autowired
    NamedParameterJdbcTemplate jdbc1;

    @RequestMapping(value = {"/load"}, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> deduction(HttpServletRequest request) {
        JSONObject jsonResult = new JSONObject();
        int pastDays = (request.getParameter("days")==null)?1:Integer.parseInt(request.getParameter("days"));
        jsonResult.put("pastDays", pastDays);
        jsonResult.put("success", true);
        messageSender.send(QUEUE_LOG_LOADER, jsonResult,3000L);
        return defaultReturn(jsonResult);
    }

    @RequestMapping(value = {"/nadyne/dlr"}, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> dlr(HttpServletRequest request) {
        JSONObject jsonResult = new JSONObject();
        jsonResult.put("success", true);

        try {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("dlr", request.getParameter("dlr"));
            params.addValue("trx", request.getParameter("trx"));

            String sql = " INSERT INTO sms_dlr_log (trx_id,dlr,ins_on) VALUES (:trx,:dlr,NOW()) ON DUPLICATE KEY UPDATE ins_on=NOW() ";
            jdbc1.update(sql, params);
            log.info("Received DLR "+request.getParameter("trx")+" : "+request.getParameter("dlr"));
        } catch (Exception e) {
            jsonResult.put("success", false);
            jsonResult.put("msg", e.getMessage());
        }

        return defaultReturn(jsonResult);
        //return new ResponseEntity<String>("ANJENK", new HttpHeaders(), HttpStatus.OK);
    }


    private ResponseEntity defaultReturn(JSONObject jsonResult) {
        HttpHeaders headers = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.OK;
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        return new ResponseEntity<String>(jsonResult.toString(), headers, httpStatus);
    }
}
