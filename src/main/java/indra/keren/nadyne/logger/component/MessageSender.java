package indra.keren.nadyne.logger.component;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import java.lang.invoke.MethodHandles;

@Service
public class MessageSender {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    private JmsTemplate jmsTemplate;


    public void send(String destination, JSONObject jsonReq, Long delay) {
        jmsTemplate.send(destination, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                Message msg = session.createTextMessage(jsonReq.toString());
                return msg;
            }
        });
    }

    public void send(String destination, JSONObject jsonReq) {
        send(destination, jsonReq, 0L);
    }
}
