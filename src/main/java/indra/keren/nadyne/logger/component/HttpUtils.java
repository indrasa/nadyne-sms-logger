package indra.keren.nadyne.logger.component;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;

@Component
public class HttpUtils {
    private final static PoolingHttpClientConnectionManager httpConnManager;
    private final static CloseableHttpClient httpClient;
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    static {
        try {
            log.info("Init HTTP client ... ");
            httpConnManager = new PoolingHttpClientConnectionManager();
            httpConnManager.setMaxTotal(100);
            httpConnManager.setDefaultMaxPerRoute(30);
            httpConnManager.setValidateAfterInactivity(30000);

            RequestConfig defaultRequestConfig = RequestConfig.custom()
                    .setSocketTimeout(30000)
                    .setConnectTimeout(30000)
                    .setConnectionRequestTimeout(30000)
                    .build();

            HttpClientBuilder builder = HttpClients.custom()
                    .setSSLHostnameVerifier(new NoopHostnameVerifier())
                    .setDefaultRequestConfig(defaultRequestConfig)
                    .disableCookieManagement()
                    .setConnectionManager(httpConnManager);

            httpClient = builder.build();
        } catch (Exception e) {
            log.error("Error: "+e,e);
            throw new ExceptionInInitializerError(e);
        }
    }

    public CloseableHttpClient getHttpClient() {
        return httpClient;
    }
    public PoolingHttpClientConnectionManager getConnectionManager() {
        return httpConnManager;
    }

    public void close(CloseableHttpResponse response) {
        try {
            EntityUtils.consume(response.getEntity());
            response.close();
        } catch (Exception e) {
            log.error("Error closing response: "+e);
        }
    }

}
