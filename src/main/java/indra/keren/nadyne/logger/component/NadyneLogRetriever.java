package indra.keren.nadyne.logger.component;

import org.apache.commons.io.IOUtils;
import org.apache.commons.text.StringSubstitutor;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.joda.time.DateTime;
import org.omg.CORBA.Environment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;
import java.lang.invoke.MethodHandles;
import java.util.*;

@Component
public class NadyneLogRetriever {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Value("${nadyne.url.login}") private String nadyneUrlLogin;
    @Value("${nadyne.url.success}") private String nadyneUrlSuccess;
    @Value("${nadyne.url.download}") private String nadyneUrlDownload;

    @Value("${nadyne.login.username}") private String username;
    @Value("${nadyne.login.password}") private String password;
    @Value("${nadyne.login.field.username}") private String usernameField;
    @Value("${nadyne.login.field.password}") private String passwordField;

    private String cookie;
    private File downloadedFile;
    private String dt1;
    private String dt2;
    private Calendar cal1;

    @Autowired
    HttpUtils httpUtils;
    @Autowired
    ZipUtils zipUtils;


}
